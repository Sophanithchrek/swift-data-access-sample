//
//  Users.swift
//  MyServerCommunication
//
//  Created by Sophanith Chrek on 24/2/21.
//

import Foundation

// MARK: UserDataResponse
struct UserDataResponse: Codable {
    
    var message: String
    var data: [Users]
    var status: Bool
    var pagination: Pagination
    
    enum CodingKeys: String, CodingKey {
        case message
        case data
        case status
        case pagination
    }
    
}

// MARK: Users
struct Users: Codable {
    
    var id: Int
    var username: String
    var gender: String
    var dateOfBirth: String
    var email: String
    var phoneNumber: String
    var imageUrl: String
    var createdDate: String
    var updatedDate: String?
    var role: Roles
    
    enum CodingKeys: String, CodingKey {
        case id = "userId"
        case username
        case gender
        case dateOfBirth
        case email
        case phoneNumber
        case imageUrl
        case createdDate
        case updatedDate
        case role
    }
    
}

// MARK: Roles
struct Roles: Codable {
    
    var id: Int
    var roleType: String
    var roleStatus: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case roleType
        case roleStatus
    }
    
}

// MARK: Pagination
struct Pagination: Codable {
    
    var page: Int?
    var limit: Int?
    var totalCount: Int?
    var totalPages: Int?
    
    enum CodingKeys: String, CodingKey {
        case page
        case limit
        case totalCount
        case totalPages
    }
    
}
