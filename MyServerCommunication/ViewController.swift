//
//  ViewController.swift
//  MyServerCommunication
//
//  Created by Sophanith Chrek on 24/2/21.
//

import UIKit

class ViewController: UIViewController {
    
    let userVM = UserViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        userVM.fetchUserData { (status) in
            if status {
                print("User data ====>\(self.userVM.users?.data)")
                
                // self.userVM.users?.data
            }
        }
        
    }


}

