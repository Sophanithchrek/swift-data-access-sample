//
//  UserViewModel.swift
//  MyServerCommunication
//
//  Created by Sophanith Chrek on 24/2/21.
//

import Foundation

class UserViewModel {
    
    let userService = UserService()
    
    let userUrl = URL(string: "http://34.87.152.68:72/api/v1/users?limit=10&page=1")

    var users: UserDataResponse?
    
    // MARK: Method to fetch all users's data
    func fetchUserData(completion:@escaping((Bool)->Void)) {
        
        userService.fetchUserData { (userDataResponse, status) in
            if status {
                self.users = userDataResponse
                completion(true)
            }
        }
        
    }
    
}
