//
//  UserSevice.swift
//  MyServerCommunication
//
//  Created by Sophanith Chrek on 24/2/21.
//

import Foundation

class UserService {
    
    let userUrl = URL(string: "http://34.87.152.68:72/api/v1/users?limit=10&page=1")
    
    // MARK: Method to fetch all users's data
    func fetchUserData(completion:@escaping((_ userDataRespose: UserDataResponse?, Bool)->Void)) {
        
        var request = URLRequest(url: userUrl!)
        request.addValue("Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJQb2xlbiBTb2siLCJleHAiOjE2MTc1MTQ3OTQsImlhdCI6MTYxMzkxNDc5NH0.lmN_TgB_zJO6Wv59XGHm283X0sGyHup7AETFKSoRAg1dMuOf7w9IaaB9pV7w4c3Fxn8NxkegHlQCuyp3ZKksSA", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if error != nil {
                
                print("Error===>\(String(describing: error?.localizedDescription))")
                completion(nil, false)
                
            } else {
                
                guard let data = data else {
                    let nsError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "error occurred during process"])
                    #if DEBUG
                   print("""
                    \(nsError.code) | \(nsError.localizedDescription)
                    """)
                    #endif
                    return
                }
                
                let decodedDataString = String(data: data, encoding: String.Encoding.utf8)?.removingPercentEncoding
                
                let responseObject = try? JSONDecoder().decode(UserDataResponse.self, from: (decodedDataString?.data(using: .utf8))!)
                
                completion(responseObject!, true)
                
            }
            
        }.resume()
    }
    
}
